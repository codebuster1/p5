package Stone;
import SpecificationsStone.FullPreciousStoneType;
import SpecificationsStone.StoneColor;

public class FullPreciousStone extends Stone {

    private FullPreciousStoneType name;

    public FullPreciousStone(){
        super();
    }


    public FullPreciousStone(double weight, double price, StoneColor color , FullPreciousStoneType name){
        super(weight, price, color);
        setName(name);
    }

    public FullPreciousStoneType getName(){
        return name;
    }

    public void setName(FullPreciousStoneType name){
        this.name = name;
    }
    @Override
    public String toString() {
        return "\nPreciousStone: " +
                "name : " + this.getName() +
                ", color : " + this.getColor() +
                ", weight : " +  String.format("%8.3f",this.getWeight()) +
                ", price : " +  String.format("%8.2f",this.getPrice()) +"kzt"+
                '\n'+'\n';
}
}
