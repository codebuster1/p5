package Stone;
import SpecificationsStone.StoneColor;

public abstract class Stone {
    private double weight;
    private double price;
    private StoneColor color;

    public Stone() {

    }

    public Stone(double weight, double price, StoneColor color) {
        setWeight(weight);
        setPrice(price);
        setColor(color);
    }


    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public double getPrice(){
        return price;
    }

    public void setColor(StoneColor color){
        this.color = color;
    }

    public StoneColor getColor(){
        return color;
    }

    @Override
    public String toString(){
        return "color - "+color+", " + "weight - "+weight+", "+ "price - "+price+"kzt"+'\n'+'\n';
    }

}
