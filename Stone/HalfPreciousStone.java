package Stone;

import SpecificationsStone.HalfPreciousStoneType;
import SpecificationsStone.Sparkle;
import SpecificationsStone.StoneColor;

public class HalfPreciousStone extends Stone {
    private HalfPreciousStoneType name;
    private Sparkle sparkle;

    public HalfPreciousStone(double weight, double price, StoneColor color) {
        super();
    }

    public HalfPreciousStone(double weight, double price, StoneColor color, HalfPreciousStoneType name, Sparkle sparkle) {
        super(weight, price, color);
        setName(name);
        setSparkle(sparkle);
    }

    public HalfPreciousStoneType getName() {
        return name;
    }

    public void setName(HalfPreciousStoneType name) {
        this.name = name;
    }

    public Sparkle getSparkle() {
        return sparkle;
    }

    public void setSparkle(Sparkle sparkle) {
        this.sparkle = sparkle;
    }


    @Override
    public String toString() {
        return "\nSemipreciousStone: " +
                "name : " + this.getName() +
                ", color : " + this.getColor() +
                ", weight : " + String.format("%8.3f", this.getWeight()) +
                ", price : " + String.format("%8.2f", this.getPrice()) +"kzt"+
                '\n'+'\n';
    }
}
