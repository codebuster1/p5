package SpecificationsStone;
public enum HalfPreciousStoneType {
    Alexandrite, Agate, Amethyst, Aquamarine, Garnet, Lapis;
}
