import Operations.Batya;
import Operations.Calculate;
import Creating.Necklace;

import java.util.Scanner;

public class App {
    public void start(){
        Necklace necklace1 = new Necklace();
        Batya batya = new Batya();
        int choice, quantityStone;
        String colour;

        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.println("Choice one operation:\n");
            System.out.println("1.Create necklace\n");
            System.out.println("2.Calculate total weight\n");
            System.out.println("3.Calculate total cost\n");

            choice = in.nextInt();
            switch (choice){
                case 1 : {
                    System.out.println("Enter the number of stones on a necklace");
                    quantityStone = in.nextInt();
                    Batya.createNecklace(necklace1,quantityStone);
                    System.out.println(necklace1+"\n");
                    break;
                }
                case 2:{
                    System.out.println("Total carat weight");
                    System.out.println(Calculate.calculateTotalCaratWeight(necklace1.getStonesList())+" carat"+"\n");
                    break;
                }
                case 3:{
                    System.out.println("Total cost in kzt");
                    System.out.println(Calculate.calculateTotalStonesCost(necklace1.getStonesList())+" kzt"+"\n");
                    break;
                }
            }
        }
    }
}
