package Creating;
import SpecificationsStone.MaterialChain;
import SpecificationsStone.Fineness;

public class Chain {

    public MaterialChain material;
    private double cost;
    private Fineness fineness;

    public Chain(){

    }

    public Chain(MaterialChain material, double cost, Fineness fineness) {
        super();
        setMaterial(material);
        setCost(cost);
        setFineness(fineness);
    }

    public MaterialChain getMaterial() {
        return material;
    }

    public void setMaterial(MaterialChain material) {
        this.material = material;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Fineness getFineness() {
        return fineness;
    }

    public void setFineness(Fineness fineness) {
        this.fineness = fineness;
    }

    @Override
    public String toString() {
        return "Chain specifications:" +
                "material : " + this.getMaterial() +
                ", cost : " +  String.format("%8.3f",this.getCost()) +"kzt"+
                ", fineness : " + this.getFineness() +
                '\n'+'\n';
    }
}