package Creating;

import Stone.Stone;

import java.util.ArrayList;
import java.util.List;

public class Necklace {
    private List<Stone> stonesList = new ArrayList<Stone>();
    private Chain chain;

    public Necklace() {

    }

    public List<Stone> getStonesList() {
        return stonesList;
    }

    public void setStonesList(List<Stone> stonesList) {
        this.stonesList = stonesList;
    }

    public Chain getChain() {
        return chain;
    }

    public void setChain(Chain chain) {
        this.chain = chain;
    }

    @Override
    public String toString() {
        return "\nCreate Necklace:\n" +
                chain +
                ", stones:  " + stonesList +
                '\n';
    }
}
