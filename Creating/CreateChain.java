package Creating;

import SpecificationsStone.Fineness;
import SpecificationsStone.MaterialChain;

import java.util.Random;

public class CreateChain {
public static Chain createChain(){
    Random r = new Random();
    return new Chain(
            MaterialChain.values()[(new Random().nextInt(MaterialChain.values().length))],
            r.nextDouble() * 10 +0.1,
            Fineness.values()[(new Random().nextInt(Fineness.values().length))]);
}
}
