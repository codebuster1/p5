package Creating;

import SpecificationsStone.FullPreciousStoneType;
import SpecificationsStone.HalfPreciousStoneType;
import SpecificationsStone.Sparkle;
import SpecificationsStone.StoneColor;
import Stone.FullPreciousStone;
import Stone.HalfPreciousStone;
import Stone.Stone;

import java.util.Random;

public class СreatureStone {
    public static Stone stoneCreate(){
        Random r = new Random();
        switch (r.nextInt(2)){
            case 0 :
                return new FullPreciousStone(
                        r.nextDouble() * 10 + 0.1,
                        (r.nextDouble() * 10 + 0.1)*1000,
                        StoneColor.values()[(new Random().nextInt(StoneColor.values().length))],
                        FullPreciousStoneType.values()[(new Random().nextInt(FullPreciousStoneType.values().length))]
                );
            case 1:
                return new HalfPreciousStone(
                        r.nextDouble() * 10 + 0.1,
                        r.nextDouble() * 1000,
                        StoneColor.values()[(new Random().nextInt(StoneColor.values().length))],
                        HalfPreciousStoneType.values()[(new Random().nextInt(HalfPreciousStoneType.values().length))],
                        Sparkle.values()[(new Random().nextInt(Sparkle.values().length))]
                );
            default:
                throw new IllegalArgumentException();
        }

    }
}
