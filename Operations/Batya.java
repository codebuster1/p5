package Operations;

import Stone.Stone;
import Creating.Necklace;
import Creating.СreatureStone;
import Creating.CreateChain;
import java.util.ArrayList;
import java.util.List;



public class Batya {

    public static Necklace createNecklace(Necklace necklace, int countStone) {
        List<Stone> stones = new ArrayList<Stone>();
        for (int i = 0; i < countStone; i++) {
            stones.add(СreatureStone.stoneCreate());
        }
        necklace.setStonesList(stones);
        necklace.setChain(CreateChain.createChain());
        return necklace;
    }


}
